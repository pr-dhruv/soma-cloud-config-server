package com.soma.somacloudconfigserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

@SpringBootApplication
@EnableConfigServer
public class SomaCloudConfigServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(SomaCloudConfigServerApplication.class, args);
    }

}
